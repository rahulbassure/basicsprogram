
package ProjectThree;
import java.util.Scanner;
public class CheckConsoVowell {
   public static void main(String args[]){
       System.out.println("Enter a Character to check if it is Conso Or Vowel");
       Scanner sc = new Scanner(System.in);
       char ch = sc.nextLine().charAt(0);
       boolean isVowel = false;
       switch(ch) 
       {
           case 'e' :
		case 'i' :
		case 'o' :
		case 'u' :
		case 'A' :
		case 'E' :
		case 'I' :
		case 'O' :
		case 'U' :  isVowel = true;		
       }
       if(isVowel == true){
           System.out.println("It is a Vowel");
          
       }
       else{
           if((ch >= 'a' && ch <= 'z' ) || (ch >= 'A' && ch <= 'Z' ) )
                   {
                       System.out.println("It is a Consonent");
                   }
           else{
               System.out.println("Input is Wrong");
           }
       }
       
       
   } 
}
