/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ProjectThree;

/**
 *
 * @author user
 */
public class MyOperatorClass {

    public void arithMaticOPs() {
        int a = 10, b = 20;
        int c = 0;
        { //Arthmetic Block
            c = a + b;
            System.out.println("Addition Operator " + c);
            c = b - a;
            System.out.println("Subtraction Operator " + c);
            a = 11;
            a++;
            System.out.println("Increment Operator " + a);
            a = 10;
            a--;
            System.out.println("Decrement Operator " + a);

        }

    }

    { //Assignment Block
        int a = 5;
        a += 3;
        System.out.println("Assignment Operator " + a);

        a = 5;
        a *= 3;
        System.out.println(a);

        double x = 5;
        x /= 3;
        System.out.println(x);

    }

    { //Comparison Operator
        int a = 5, b = 6;
        System.out.println(a != b);
        System.out.println("a<=b " + "" + (a <= b));
    }

    {  //Logical operator
        int a = 5, b = 10;
        System.out.println(" a>10 && a<5 is " + " " + (a > 10 && a < 5));
        System.out.println(" a>2 || a<10" + " " + (a > 2 || a < 10));

    }
    {//ternury operator
        int a=2;  
        int b=5;  
        int max=(a<b)?b:a;  
        System.out.println("max " + max); 
        int marks =45;
        String result = (marks > 40) ? "pass" : "fail";
        System.out.println(result);
//     String ars =( marks > 40 ? System.out.println("pass") : System.out.println("fail"); )
    }

    public static void main(String args[]) {
        MyOperatorClass moc = new MyOperatorClass();
        moc.arithMaticOPs();
    }
}
